Meadowthorpe is an assisted living, memory care, and respite care facility in Lexington, KY. Our community is a five-cottage style campus that includes eight apartments in each cottage. We offer private one bedroom apartments that are designed to be home-like.

Address: 191 Leestown Center Way, Lexington, KY 40511, USA

Phone: 859-878-1300

Website: https://meadowthorpe.com
